# -*- coding: utf-8 -*-
# RafKac
# 2021_08_01
# 2021_08_02
# na podstawie filmików instruktarzowych z yt
#
# Interfejs graficzny z modułem Tkinter
# Canvas w module Tkinter
# manager Grid - rozmieszczenie elementów w ramce
# obsługa zdarzeń w interfejsie graficznym
# dodawanie ikon/grafiki do interfejsu graficznego
# interfejs graficzny

import tkinter
from PIL import Image, ImageTk


def funkcjaPrzycisku():
    print("Wciśnięto przycisk.")


def funkcjaPrzycisku2():
    print("Wciśnięto 2")


def prawy(event):
    print("Wciśnięto 3 - PPM")


def funkcjaPrzycisku3(event):
    print("Wciśnięto 3")


def funkcjaOkna(event):
    print("Kliknięto na okno.")


root = tkinter.Tk()
root.geometry('1000x600')

'''
l1 = tkinter.Label(root, text="Aplikacja")
l1.pack()

b = tkinter.Button(root, text='Jestem przyciskiem1', width=10, bg="red", fg="white", command=funkcjaPrzycisku)
b.pack()
b2 = tkinter.Button(root, text='Jestem przyciskiem 2', command=funkcjaPrzycisku)
b2.pack(side=tkinter.RIGHT)
b3 = tkinter.Button(root, text='Jestem przyciskiem 3', command=funkcjaPrzycisku)
b3.pack(side=tkinter.BOTTOM)
b4 = tkinter.Button(root, text='Jestem przyciskiem 4', command=funkcjaPrzycisku)
b4.place(x=350,y=300)
'''

#canvas w module Tkinter
'''
c = tkinter.Canvas(root, height=300, width=350, bg="green")
c.pack()

for i in range(5):
    c.create_line(0, 30*i, 350, 30*i, width=10)

c.create_oval(30, 30, 150, 150, fill='red')
c.create_rectangle(20, 200, 100, 300, fill="blue")
'''

# manager Grid
'''
tkinter.Label(root, text='Login').grid(row=0, column=0, padx=2, pady=2)
tkinter.Entry(root).grid(row=0, column=1)

tkinter.Label(root, text='Hasło').grid(row=1, column=0, padx=2, pady=2)
tkinter.Entry(root).grid(row=1, column=1)

b1 = tkinter.Button(root, text="Wyślij", command=funkcjaPrzycisku).grid(row=2)

# obsługa zdarzeń w interfejsie graficznym

b2 = tkinter.Button(root, text="Przycisk2", command=funkcjaPrzycisku2).grid(row=3)
b3 = tkinter.Button(root, text="Przycisk3")
b3.bind('<Button-1>', funkcjaPrzycisku3)
b3.bind('<Button-3>', prawy)
b3.grid(row=4)

root.bind('<Button-1>', funkcjaOkna)
'''

# dodawanie ikon/grafiki do interfejsu graficznego
'''
zdjecie = Image.open('grafika.jpg')
zdjecie = zdjecie.resize((150, 100), Image.ANTIALIAS)
ikona = ImageTk.PhotoImage(zdjecie)
tkinter.Label(root, image=ikona).pack()

canvas = tkinter.Canvas(root, width=300, height=300, bg="blue")
canvas.pack()
canvas.create_image(30, 30, anchor=tkinter.NW, image=ikona)
'''

# interfejs graficzny

glowneMenu = tkinter.Menu()
root.config(menu=glowneMenu)

fileMenu = tkinter.Menu(glowneMenu)
glowneMenu.add_cascade(label="File", menu= fileMenu)
fileMenu.add_command(label='New File')
fileMenu.add_command(label='Open')
fileMenu.add_command(label='Open Module')
fileMenu.add_separator()
fileMenu.add_command(label='Recent File')

editMenu = tkinter.Menu(glowneMenu)
glowneMenu.add_cascade(label="Edit", menu= editMenu)
editMenu.add_command(label='Undo')
editMenu.add_command(label='Redo')

podMenu = tkinter.Menu(editMenu)
editMenu.add_cascade(label="Male", menu= podMenu)
podMenu.add_command(label='Undo')
podMenu.add_command(label='Redo')


root.mainloop()