# -*- coding: utf-8 -*-
# by RafKac
# 27-28 maja 2021
# zadanie powstało jako sortowanie przez wstawianie, ulepszone o dodatkowe możliwości

def generujTablice():
    import random
    prob = random.randrange(10,30)
    tab = []
    for i in range(0,prob):
        l = random.randrange(1,200)
        tab.append(l)
    return tab


def minimum(tab):
    min = 0
    for i in range(0, len(tab)):
        if(tab[i] < min):
            min = tab[i]
    return min


def maximum(tab):
    max = 0;
    for i in range(0, len(tab)):
        if(tab[i] > max):
            max = tab[i]
    return max
    


def sortPrzezWstawianie():
    print("Sortowanie przez wstawianie: ")
    #tabP = [14, 2, 34, 198, 4, 23, 2, 56, 34]
    tabP = generujTablice()
    print(tabP)
    wartownik = 0
    wynikowa = []
    if(tabP[0] < tabP[1]):
        wynikowa.append(tabP[0])
        wynikowa.append(tabP[1])
    else:
        wynikowa.append(tabP[1])
        wynikowa.append(tabP[0])
    print("Wynikowa na początku: {}".format(wynikowa))    
    for i in range(2,len(tabP)):
        wartownik = 0
        if(tabP[i] < wynikowa[0]):
            wynikowa.insert(0, tabP[i])
            print("Dodajemy pierwszą wartość: {}, {}".format(tabP[i],wynikowa))
            continue
        for j in range(0,len(wynikowa)):
            print("Wynikowa: {}, i: {}, j: {}, tabP[i]: {}, wartownik: {}".format(wynikowa, i, j, tabP[i], wartownik))
            if(wynikowa[j] <= tabP[i]):
                wartownik = j
                print("Zmiana wartownika: {}".format(wartownik))
            else:
                wynikowa.insert(wartownik + 1 ,tabP[i])
                print("dodajemy środkowe - wartownik: {}, wartość: {}, i: {}, j: {} ".format(wartownik, tabP[i],i,j))
                break
            if(wartownik == len(wynikowa)-1):
                wynikowa.append(tabP[i])
                print("dodajemy końcowe: wartość: {}, wartownik: {}, i: {}".format(tabP[i], wartownik, i))
    print("\nPo posortowaniu: {}".format(wynikowa))


#print("Zadanie - sortowanie")
sortPrzezWstawianie()
print("Koniec")
