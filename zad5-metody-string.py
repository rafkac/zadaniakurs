# -*- coding: utf8 -*-
#zadania 1-3 z lekcji piatej kursu flynerd
#17 maja 2021

#zad1
sentence = "Kurs Pythona jest prosty i przyjemny"
iloscZnakow = len(sentence)
print("Ilosc znakow w napisie: {}".format(iloscZnakow))
sentence.split(" ", sentence.count(" "))
tab = sentence.split(" ", 5)
print(tab[3])
print("Znaki o indeksie: \n 7: {}, \n 12: {},\n -4: {},\n 37: nie istnieje".format(sentence[7], sentence[12], sentence[-4] ))

print("Wprowdzamy dwa bledy ortograficzne:")
sentence2 = sentence.replace('h','ch')
sentence3 = sentence2.replace('rz','sz')
print(sentence3)

#zad2 - pytanie o dane i ich przetwarzanie
imie = input("Podaj imie:")
nazwisko = input("Podaj nazwisko:")
tel = input("Podaj numer telefonu:")

#sprawdzamy poprawnosc podanych ciagow znakow
print("poprawnosc imienia: ",imie.isalpha())
print("poprawnosc nazwiska: ",nazwisko.isalpha())
print("poprwnosc numeru telefonu: ",tel.isdigit())

#poprawiamy, aby imie i nazwisko zaczynalo sie duza litera
imie = imie.capitalize()
nazwisko = nazwisko.capitalize()
print("Poprawne imie i nazwisko: {} {}".format(imie, nazwisko))


#jesli w numerze telefonu sa jakies znaki inne niz cyfry, to usuwamy
if tel.isdigit() == False:
    tel = tel.replace('-','')
    tel = tel.replace(' ','')
    print(tel)

#przy zalozeniu polskiego imienia sprawdzamy, czy user jest kobieta
if imie.endswith('a'):
    print("uzytkownik jest kobieta")

#laczymy dane w jeden ciag "personal" za pomoca spacji
personal = imie + " " + nazwisko + " " + tel
print("pelne dane osobowe {}, wszystkie znaki: {}, litery: {}".format(personal, len(personal), len(personal[:-9]) -2 ))



# zad3
# czyli zabawa w bioinformatyka
print("************************************** \n zadanie 3 - bioinformatyka")
adenina = 'A'
cytozyna = 'D'
guanina = 'G'
tymina = 'T'

DNA = "ACTGTGCTGACTCCCGGTGCTGCCGCTGCCATAGCTAAAGCCCGGGTCCTGGTAGGCAGGCGGGAAGCAG00GGTGGGGGTCCCGGGTACTGGTAGGGGTAGCCCTGACCCAGAGGCGGGGGGGCAGCCGGGTGGGGCAGCGGGGCCAGCGTGTCCTGAA-CGAAGTCCCACTGGAGCCACTGTTGAGGTTCAGGGTGGCGAGATCTGGCGGNNNAGGGTAGGTGAGGGCCGCGGAGGGGCCTCCGGCGTTCCCCTCCCCCCCGCCCTGAAACCCGAAGCCCCCACTCACTGCTGCAGAGATCCCCTGAAAACGTAGTAGCACTGCTCgagacAGGTGATCTGTTGACCTGAAACCGCAGGAAGCCGTGCTTCAGCAAGCTGCTGGCGTACTTCCGGGCCT---GCCGCTCCTTGAAGCCCTCCACGTGTGTGTACAGCCAGTCCACCACGTCCGCCCCTGGCCGGCACCAGCGGTCAGCCCGCAGCCTCGAGGCAAGCAGCCCTGCCNNTGGCACTATCCGC-CGCGGGGACGGCCACTCACCGATGACGGCATNNGCGATGGTGATCTTGAGCCACATGCGGTCGCGGATCTCCAGTCCCGAG---GGCAGCTGCATGACCCGGACGACGGCGCTCATGTCACtcaccgtcagcggcgcctcttccagCCAGCTCTGCAAAGCACAGACAGCCCCGCTTCGCCCCAGCATCTGAAAGCGGGGGACTCggcAcgCTGCACCCCCAGGGGAGCCTCTGGGCAGAGCCTGCGCCAGGGCGCAAGCTGGACGGTGCGTGACAGCAGGGCCCCGGCCCACTGCAGGATGCACCCCCGTGAGGCTGGGGCGTGAGCAGGGGGGTTGGACAtttAGTCTCCCACTTCTACAGACACTTTTCATCAGGATCCTAGGCACAAACTGGGCTGAAACCCCACCCTGCAGACCAGGAAGTAATGAGAACAGGGCAGGCCCCTTCCCCTCNNCGCATGCC-CACCCGAGAGCGCAGGCTGTTAGTCGTGTTAATGGCAGGAAGCAGAATGGAGACCTGGCCCCTGCCTCTGAA-CCGTGGGTGCTCaactggctaGCCCTACGTACATCCCCTGTTCcggCCAACACACAGACATGAGCAGGATGGGCTGCACAAGGTGGGCACGGGTGCCTGTGCACACGTCTGTGCAGGGAGTTGGGGACAGGCAACACACACGTGTCACAGCCCCATGACGGggcaattgcGCCATGCTGGCTGAATGGCAGAGACGCCCCTCCAAGCCTCGGTTTCTGCTGGGGCCCTCAGGAGCTGCCACTTACGTGGAGCACCAGGCACGGAGCTGGTTAGTGAGGAGGAGCTGGTGCGCGTGACGGCGCTGGAGCAGGGACTCGTACCGTAGCGGGGCAGGGCNNNTGTCAGTGCCGCCGTGTGGtcagcggcgatCGGCG-GGTCGATGGGCCGCACCGGGTCAGCTGGGTGNAGACACGTGGCGATGACAGGCGGACAGATGGACAGGGTGGGAGGGCAGGGTGCAGGGCACAGAGGAGAGAGGCCTTCAGGCTAGGTAGGCGCCCCCTCCCCATCCCGccccGTGTGCCCCGAGGGCCACTCACCCCGTGGGACGGTGAAGTAGCTTCG-GGCGTTGGGTCCAGCACTTGGCCACAGTGAGGCTGNAAATGGCTGCAGGAACGGTGGTCCCCCCGCAAGGCCCCCATGGTCCCACCTCCCTGCCTGGCCCCTCCCGCTCCAGCGCCNCCAGCC"

print("Wystepowanie zasad azotowych: \n adenina: {}, \n cytozyna: {}, \n guanina: {}, \n tymina: {}".format(DNA.count(adenina), DNA.count(cytozyna), DNA.count(guanina), DNA.count(tymina) ))

print("Ilosc wystapien sekwencji \n GAGA: {}, \n AGAG {}".format( DNA.count("GAGA"), DNA.count("AGAG") ) )

print("7 guanin z rzedu wystepuje od indeksu: {}".format( DNA.find("GGGGGGG") ))
print("6 cytozyn od konca lancucha wystepuje od indeksu: {}".format( DNA.rfind("DDDDDD") ))

print("Sekwencja CTGAAA pojawila sie w kodzie {} razy".format( DNA.count("CTGAAA") ))

print("Sekwencja CTGAA- pojawila sie w kodzie {} razy".format( DNA.count("CTGAA-") ))

czysteDNA = DNA.replace("-","")
czysteDNA = czysteDNA.replace("N","")
czysteDNA = czysteDNA.upper()

print("Czyste DNA: \n {}".format( czysteDNA ))

print("Nic RNA: \n {}".format( czysteDNA.replace('T', 'U') ) )



