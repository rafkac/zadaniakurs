# -*- coding: utf-8 -*-
# rafkac
# 2021_06_02
# kolekcje - słownik
# krotki i wycinki
# wyrażenia listowe, string formatter


'''
#słownik i krotki
slownik = {1: "Poniedziałek", 2: "Wtorek", 3: "Środa", 4: "Czwartek", 5: "Piątek", 6: "Sobota", 7: "Niedziela"}


print(slownik[1])
print(slownik[7])
slownik[8] = False
slownik[9] = 5
slownik["a"] = 1
print(slownik["a"])
print(slownik.get(11, "Inny dzień"))
print(slownik)

print("\nPętla: ")
for l in slownik:
    print(l)
for l in slownik.values():
    print(l)

print("\nPo usunięciu: ")    
slownik.pop(1)
print(slownik)
'''

'''
# wycinki
krotka = (2, 4, 8, 16, 32, 64, 128)
print(krotka[0])
print(krotka[6])
print(krotka)
print("Elementów: ", krotka.count(2))
print("Index: ", krotka.index(64))

print("\nWycinki:")
print(krotka[0:3])
print(krotka[3:5])
print(krotka[0:100])
print(krotka[-4:-2])
print(krotka[0:7:2])
print(krotka[::-2])
'''

#wyrażenia listowe, string formatter
lista = list(range(10))


print(lista)
nowa = [i * 2 for i in lista]
nowa2 = [i + 2 for i in lista if i % 2 == 0]
print("Nowa: ", nowa)
print("Nowa2: ", nowa2)

argumenty = ["Sebald", 24]
tekst = "Cześć mam na imię: {0} i mam {1} lat.".format(argumenty[0],argumenty[1])
arg2 = ["Paco", 34]
tekst2 = "Cześć, mam na imię {imie} i mam {wiek} lat.".format(imie = arg2[0], wiek = arg2[1])
print(tekst)
print(tekst2)
