#-*- coding: utf-8 -*-
# zadanie 2 - kalkulator kalorii i BMI
# 2021_04_29
masa = float(input("Podaj Twoją wagę (w kilogramach):"))
wzrost = int(input("Podaj Twój wzrost (w centymetrach):"))
wiek = int(input("Podaj Twój wiek:"))
S = int(input("Podaj współczynnik S (5 dla mężczyzn, -161 dla kobiet)"))
wskaznik = float(input("Podaj wskaźnik aktywności fizycznej: "))
print("Twoje zapotrzebowanie wynosi: ", (10*masa + 6.25 * wzrost - 5*wiek + S) * wskaznik,"kcal")

print("Teraz policzymy Twoje BMI:")
bmi = masa * 10000 / wzrost**2
print("Twoje bmi wynosi:", bmi)
