# -*- coding: utf-8 -*-
# RafKac
# 2021_08_02
# 2021_08_03
#
# wykresy z Matplotlib
# układ dwóch i trzech równań liniowych
# pochodne funkcji jednej zmiennej


import numpy
import matplotlib.pyplot as plot
import sympy
import scipy.misc

'''
x = numpy.linspace(-3, 3, 100)
y1 = numpy.sin(x)
y2 = numpy.exp(x)

plot.plot(x, y1, color='green', marker='o')
plot.plot(x, y2, color="blue", marker='+', linewidth=0.15, linestyle='none')
plot.xlabel('argumenty')
plot.ylabel("Wartości")
plot.legend(['sin(x)', 'exp(x)'])

plot.ylim([-1, 1])
plot.savefig("Wykresik.png")


plot.show()
'''

# układ dwóch i trzech równań liniowych

A = numpy.array([[1, 3], [3, -6]])
B = numpy.array([11, 3])
Z = numpy.linalg.solve(A, B)
print(Z)
A2 = numpy.array([[1, -7, 3], [2, 5, -1], [1, 1, 1]])
B2 = numpy.array([2, -8, 5])
Z2 = numpy.linalg.solve(A2, B2)
print(Z2)

# pochodne funkcji jednej zmiennej

def funkcja(x):
    return 5*pow(x, 2) - x + 16

x = sympy.Symbol('x')
print(sympy.diff(5*pow(x, 2) - x + 16, x))

print(scipy.misc.derivative())

z = numpy.linspace(-5, 5)
plot.plot(z, funkcja(z))
plot.plot(z, scipy.misc.derivative(funkcja, z))


plot.show()





# aplikacja saper 2021_08_04


