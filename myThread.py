# -*- coding: utf-8 -*-
# RafKac
# 2021_07_28
# klasa implementująca nasze wątki, na podstawie filmiku instruktażowego z yt

import threading
import time


class myThread(threading.Thread):
    def __init__(self, nazwa="myThread", opoznienie=1, powtorzenia=3):
        threading.Thread.__init__(self)
        self.nazwa = nazwa
        self.opoznienie = opoznienie
        self.powtorzenia = powtorzenia

    def run(self):
        print(self.nazwa, " rozpoczyna działanie.")
        for i in range(self.powtorzenia):
            print(self.nazwa, " usypia na ", self.opoznienie, " sekund.")
            time.sleep(self.opoznienie)
        print(self.nazwa, " kończy działanie.")


print("Tworzymy wątki: ")
watek1 = myThread("PierwszyW", 1, 8)
watek2 = myThread("DrugiW", 2, 3)
watek1.start()
watek2.start()
watek1.join()
watek2.join()
print("koniec, już po wątkach")