# *-* coding: utf-8 *-*
# RafKac
# 2021_06_09
# operacje na tekście i listach
# analiza tekstu
# funkcje anonimowe, lambda
# mapa i filtr
# generatory

'''
print(", ".join(["a", "b", "c"]))
print("Krótki tekst".replace("Krótki", "Zmieniony"))
print("To jest zdanie".startswith("To"))
print("To jest zdanie".endswith("."))
print("j" in "To jest zdanie")
print("To jest zdanie".upper())
print("To jest zdanie".lower())

print("----------------\n")
lista = [10, 20, 25, 35, 40]
if (all([i % 2 == 0 for i in lista]) ):
    print("Wszystkie parzyste")
else:
    print("Nie wszystkie są parzyste")

if (any([i % 2 == 0 for i in lista])):
    print("Conajmniej jeden argument z listy jest parzysty.")

for i in enumerate(lista):
    print(i)
    print(i[0] + 1, "-", i[1])

'''

# analiza tekstu
'''
plik = open("test.txt", "r")
tekst = plik.read()
plik.close()

def policz(txt, znak):
    licznik = 0
    for z in txt:
        if( z== znak):
            licznik += 1
    return licznik

print(policz(tekst, "a") + policz(tekst, "A"))
print(policz(tekst.lower(), "a"))
print(policz(tekst.lower(), "z"))

for z in "abcdefghijklmnoprstuwxyz":
#    print(z, " ~ " ,policz(tekst.lower(), z))
    ile = policz(tekst.lower(), z)
    procent = 100 * ile / len(tekst)
    print("{0} - {1} - {2}%".format(z.upper(), ile, round(procent, 3) ))
'''

# funkcje anonimowe, lambda
'''
def funkcja(f, liczba):
    return f(liczba)


print(funkcja(lambda x: x * x, 6))


def kwadrat(x):
    return x * x

print(kwadrat(5))

wyn = (lambda x : x * x)
print(wyn(6))
print(wyn(8))

lam2 = lambda x, y: x * y
print(lam2(2, 5))
print((lambda x, y : x + y)(5, 6))

'''

# mapa i filtr
'''
liczby = [2, 10, 12, 15, 20, 25, 30, 35]

# ###mapy

def funkcja(x):
    return x * 2

wynik = map(funkcja, liczby)
print(list(wynik))

# # to samo działanie z wykorzystaniem wyrażenia lambda

wynik2 = map(lambda x: x + 2, liczby)
print(list(wynik2))

# ## filtry

wynik3 = filter(lambda x: x % 2 == 0 , liczby)
print(list(wynik3))
'''


# generatory
def gen():
    i = 0
    while i < 5:
        yield i
        i += 1


def parzyste(x):
    i = 0
    while i <= x:
        if( i % 2 == 0):
            yield i
        i += 1    
    
        
for i in gen():
    print(i)

for i in parzyste(16):
    print(i)

print(list(gen()))
print(list(parzyste(40)))
