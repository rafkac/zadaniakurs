# -*- coding: utf8 -*-
# zadania z lekcji 7 - pętla "for"
# 2021_05_21

#Zad 1 - suma poprzedników
'''
for i in range(1,11):
    suma = 0
    for j in range(0,i):
        suma = suma + j

    print("Suma poprzedników dla {} wynosi {}".format(i, suma))    
'''

# Zad 2 - sześcian kolejnych liczb naturalnych
'''
for i in range(1, 11):
    print("{} => {} ".format(i , i**3))
'''

# Zad 3 - powitanie każdej osoby z listy
'''
print("Imiona:")
tab = []
while(True):
    znak = input("Podaj imie (N, aby zakończyć):")
    if znak=='N':
        break
    tab.append(znak)
    
print(tab)    

for i in range(0, len(tab)):
    print("Dzień dobry, {}".format(tab[i]))
'''

# Zad 4 - zabawy z pętlą:
'''
liczba = int(input("Podaj liczbę: "))
czyTrzy = False
czyCztery = False

for i in range(0, liczba):
    print(i)
    if((i % 3) == 0):
        print("Tak - Liczba {} jest wielokrotnością 3".format(i))
        czyTrzy = True
    else:
        print("Liczba {} NIE jest wielokrotnością 3.".format(i))
        czyTrzy = False

    if((i % 4) == 0):
        print("Tak - Liczba {} jest wielokrotnością 4".format(i))
        czyCztery = True
    else:
        print("Liczba {} NIE jest wielokrotnością 4.".format(i))
        czyCztery = False    
    if(czyTrzy and czyCztery):
       print("hurra")
    if((czyTrzy == False) and (czyCztery == False)):
        print("*")
'''

# Zad 5 - choinka, w której każdy poziom jest większy o jeden wiersz
'''
poziom = int(input("Ile poziomów ma mieć choinka?"))
for i in range(1, poziom+1):
    #print("Zaczynamy kolejny poziom:")
    for j in range(1, i+1):
    #    print("Zaczynamy kolejny trójkąt:")
        #for k in range(1, j+1):
        print((j) * "#")
'''
# Zad 6 - klasyczna tabliczka mnożenia

for i in range(1,11):
    for j in range(1,11):
        print("{:2} * {:2} = {:2}".format(i, j, i*j))
    print()
