# -*- coding: utf-8 -*-
# RafKac
# 2021_08_12
# 2021_09_13
#
# na podstawie filmiku z yt:
# https://www.youtube.com/watch?v=s7R5KThCbGo   data i czas
# https://www.youtube.com/watch?v=fwMyIL6LRA8   podstawowe operacje na plikach
# https://www.youtube.com/watch?v=_c-ESIceGHg   operacje na katalogach

import time
import datetime as dt
import os


#print("start")
#time.sleep(2)
#print("Stop")

timer = time.time()
timer2 = time.time()
print(timer)
time.sleep(2)
elapsed = time.time()- timer
print(elapsed)
'''
while True:
    if time.time() - timer > 1:
        print("1 sek")
        timer = time.time()
    if time.time() - timer2 > 5:
        print("Koniec")
        break

d1 = dt.datetime(year=2021, month=3, day=10)
teraz = dt.datetime.now()
print(d1)
print(teraz)
print(teraz.hour)
print(teraz.strftime("%H:%M %d.%m.%Y"))
'''

# operacje na plikach

'''
f = open("test.txt", "r+")

f.write("cwks\n")
f.seek(6)
przeczytane = f.read(5)
calyPlik = f.readlines()
print(calyPlik)
for line in calyPlik:
    print("linia bez końca wiersza")
    print(line, end="")
    print("używając rstrip")
    print(line.rstrip())

f.close()
'''

# moduł os, tworzenie folderów, listy plików

lista = os.listdir(".")
print(lista)
for l in lista:
    print(l, end="")
    if os.path.isfile(l):
        print(" - plik")
    if os.path.isdir(l):
        print(" - katalog")
    if os.path.islink(l):
        print(" - dowiązanie")

path = "pliki/01/dane.txt"
plik = open("test1.txt", "w").close()

#os.makedirs(path)
#open(path, "w").close()
print(path)
print(os.path.dirname(path))
print(os.path.basename(path))
print(os.path.abspath(path))

# Tworzenie:
dir_path = os.path.dirname(path)
os.makedirs(dir_path)
open(path, "w").close()