# *-* coding: utf-8 *-*
# RafKac
# 2021_06_17
# 2021_06_29
# 2021_06_30
# 2021_07_01
# 23. dekoratory
# 24. zbiory
# 25. obiektowość
# 26. klasy - dziedziczenie
# 27. Klasy - magiczne metody
# 28. Klasy - cykl życia (destruktor)
# 29. klasy - hermetyzacja

'''
def decorator(func):
    def wrapper():
        print("-------")
        func()
        print("-------")
    return wrapper
    
def hello():
    print("Hello world!")



hello()    
hello = decorator(hello)
hello()
print()

@decorator
def witaj():
    print("WItaj świecie!")

witaj()
'''

#zbiory (sets)
'''
liczby1 = {0, 1, 2, 3, 4, 5, 6, 7, 9}
slowa = set(["a", "b", "c"])


print(liczby1)
print(slowa)

liczby1.add(5)
print(liczby1)
liczby1.remove(0)
print(liczby1)
liczby1.add(5)
print(liczby1)

print(1 in liczby1)
print("a" in liczby1)

liczby1 = {0, 1, 2, 3, 4, 4 }
liczby2 = {3, 4, 5, 6}

print(liczby1 | liczby2)
print(liczby1 & liczby2)
print(liczby1 - liczby2)
print(liczby2 ^ liczby1)
'''

#klasy i obiekty, obiektowość
'''
class Czlowiek:

    def __init__(self, imie, wiek):
        self.imie = imie
        self.wiek = wiek

    def przedstawSie(self, powitanie = "Cześć"):
        return powitanie + ", mam na imię " + self.imie + " lat " + str(self.wiek)
        
    def setImie(self):
        imie = input("Podaj nowe imię:")
        self.imie = imie



obiekt = Czlowiek("Sebastian", 19)
print(obiekt.imie)
#print(obiekt.przedstawSie("Witam"))
obiekt2 = Czlowiek("Adrian", 99)
obiekt2.imie = "Adrian"
#print(obiekt2.przedstawSie())
#print(obiekt.przedstawSie())
'''

# klasy - dziedziczenie
'''
class Animal:
    def __init__(self, name, age):
        self.name = name
        self.age = age

        
class Dog(Animal):
    def voice(self):
        print("Hał hał")


class Wolf(Dog):
    def getVoice(self):
        print("Jestem wilkiem, ")
        super().voice()


class Cat(Animal):
    def bite(self):
        print("Zaraz Cię podrapię!!")

    def getVoice(self):
        print("Miał miał.")
        

dog = Dog("Reksio", 10)        
print(dog.name + " w wieku " + str(dog.age))
dog.voice()

cat = Cat("Burek", 3)
cat.getVoice()

wolf = Wolf("Geralt", 55)
wolf.getVoice()
print(wolf.name)
'''

# klasy - metody magiczne
'''
import math

class Punkt2D:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.odleglosc = math.sqrt(x**2 + y**2)


    def __add__(self, drugi):
         return Punkt2D(self.x + drugi.x, self.y + drugi.y)  


    def __sub__(self, drugi):
        return Punkt2D(self.x - drugi.x, self.y - drugi.y)

    #operator "mniejsze od", za taki traktujemy ten leżący bliżej środka układu współrzędnych
    def __lt__(self, drugi):
        return self.odleglosc < drugi.odleglosc


    #operator "mniejsze bądź równe" (not equal)
    def __ne__(self, drugi):
        return self.odleglosc <= drugi.odleglosc

    #operator "równe"
    def __eq__(self, drugi):
        return self.x == drugi.x and self.y == drugi.y

    def __len__(self):
        return int(round(self.odleglosc, 0))


p1 = Punkt2D(2,5)
p2 = Punkt2D(4,5)
p3 = p1 + p2
print(p3.x)
print(p3.y)
print(p1 < p2)
print(p1.odleglosc)
print(p2.odleglosc)
print(p3 < p2)
print(p1 == p2)
print(p2 == p2)
print(len(p3))
print(p3.odleglosc)
'''

# klasy - cykl życia (destruktor)
'''
class Test:
   # def __new__(cls):
   #     print("Hello Class")

    def __del__(self):
        print("      Bye Class")


obj = Test()
obj2 = obj
lista = [obj2]
del obj
del obj2

print("      Koniec")
'''
# klasy - hermetyzacja

class Test:
    __lista = []
    def dodaj(self, arg):
        self.__lista.append(arg)

    def zdejmij(self):
        if len(self.__lista)> 0:
            return self.__lista.pop(len(self.__lista) - 1)
        else:
            return


obj = Test()
obj.dodaj("A")
obj.dodaj("B")
obj.dodaj("C")
print(obj.zdejmij())
#obj.lista.append("X")
print(obj._Test__lista)    






        
