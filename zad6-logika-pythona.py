# -*- coding: utf8 -*-

#Zadania z lekcji 6 - czyli instrukcje warunkowe.
# Zad 1 - skrypt pytający użytkownika o wiek i komentujący to odpowiednio
'''
wiek = int(input("Ile masz lat?"))
if (wiek < 18):
    print("Użytkownik niepełnoletni, brakuje Ci {} lat, poczekaj.".format(18 - wiek))
else:
    print("Użytkownik pełnoletni.")
    if(wiek > 100):
        print("200 lat")
                 
'''

#Zad 2 - znów kalkulator BMI, z interpretacją wyników
'''
print("******************************* \n Zadanie 2")
waga = float(input("Podaj Twoją wagę: (w kg)"))
wzrost = float(input("Podaj Twój wzrost: (w m)"))

BMI = waga/wzrost **2
print("Twoje BMI wynosi {:4.2f}".format(BMI))
if (BMI < 18.5):
    print("Masz niedowagę.")
elif(18.5 <= BMI < 24):
    print("Waga normalna.")
elif(24 <= BMI < 26.5):
    print("Lekka nadwaga.")
else:
    print("Masz nadwagę")
    if(30 <= BMI < 35):
        print("Otyłość I stopnia.")
    elif(35 <= BMI < 40):
        print("Otyłość II stopnia.")
    elif(BMI >= 40):
        print("Otyłośc III stopnia.")
'''


#Zad3 - sortowanie trzech liczb
'''
print("********************************* \n Zadanie 3 - sortowanie trzech liczb")        
digA = float(input("Podaj pierwszą liczbę:"))
digB = float(input("Podaj drugą liczbę:"))
digC = float(input("Podaj trzecią liczbę:"))

if(digA > digB):
    if(digA > digC):
        if(digC > digB):
            print(digA, digC, digB)
        else:
            print(digA, digB, digC)
    else:
        print(digC, digA, digB)
else:
    if(digB > digC):
        if(digC > digA):
            print(digB, digC, digA)
        else:
            print(digB, digA, digC)     
    else:
        print(digC, digB, digA)
'''


#Zadanie 4 - zbiór imion, sprawdzamy, czy imie jest żeńskie, jeśli nie, to można je dodać + komunikat
'''
name = ["Adam","Basia","Ola","Ala","Kinga","Michał","Bob","Bogumił","Bożydar"]

imie = input("Podaj imię:")
if(imie not in name):
    print("Nie znam tego imienia.")
    name.append(imie)

if(imie[-1] == 'a'):
    print("Imię kobiece.")
else:
    print("Imię męskie.")

print(name)
'''

#Zadanie 5 - twierdzenie Pitagorasa

A = int(input("Podaj długość boku A:"))
B = int(input("Podaj długość boku B:"))
C = int(input("Podaj długość boku C:"))

lista = [3.0/4.0, 4.0/5.0, 5.0/4.0, 4.0/3.0, 3.0/5.0 ]
print(lista)
print(A/B, B/C, C/A)

if(A + B > C and A + C > B and B + C > A):
    print("\n Jest mozliwe utworzenie trójkąta \n")
    if(A*A + B*B == C*C or A*A + C*C == B*B or C*C + B*B == A*A):
        print("Trójkąt pitagorejski")
        if((A/B in lista) and (B/C in lista)):
            # powyższy warunek może jeszcze mieć wygląd taki:
            # a/3 == b/4 == c/5
            print("Trójkąt egipski.")
    else:
        print("To NIE jest trójkąt pitagorejski")
else:    
    print("Nie jest możliwe utworzenie trójkąta")



