# *-* coding: utf-8 *-*
# RafKac
# 2021_07_02
# 2021_07_07
# 2021_07_09
# 2021_07_12

# 30. Klasy - metody klas oraz metody statyczne
# 31. Klasy - właściwości (getter, setter)
# 32. Wyrażenia regularne cz.1 - match, search
# 33. Wyrażenia regularne cz.2 - klasy znaków, przedziały
# 34. wyrażenia regularne 3 - powtórzenia
# 35. wyrażenia regularne 4 - grupt, walidaje email
# 36. argumenty funkcji (lista parametrów)
# 37. Rozpakowanie krotki.
# 38. Skrócony operator IF

# klasy - metody klas oraz metody statyczne
'''
class Czlowiek:
    def __init__(self, imie):
        self.imie = imie

    def przedstaw(self):
        print("Nazywam sie " + self.imie)

    @classmethod
    def nowy_czlowiek(cls, imie):
        return cls(imie)

    @staticmethod
    def przywitaj(arg):
        print("Cześć " + arg)
        

obj = Czlowiek("Hipolit")
cz1 = Czlowiek.nowy_czlowiek("Hubert")

obj.przedstaw()
cz1.przedstaw()
cz2 = cz1.nowy_czlowiek("Adrian")
cz2.przedstaw()
cz2.przywitaj("Adrian")
Czlowiek.przywitaj("ty stary patologu!!!!")
'''

# klasy - właściwości (getter, setter)
'''
class KontoBankowe:
    __stan = 0


    
    @property
    def stan_konta(self):
        return self.__stan

    @stan_konta.getter
    def stan_konta(self):
        return "Stan konta: " + str(self.__stan) + " pln."

    @stan_konta.setter
    def stan_konta(self, value):
        self.__stan += value




konto = KontoBankowe()
print(konto.stan_konta)
    
konto.stan_konta = 50 
print(konto.stan_konta)

konto.stan_konta = 500 
print(konto.stan_konta)

konto.stan_konta = -550 
print(konto.stan_konta)
'''

# wyrażenia regularne
'''
import re

wzor = r"banan"
tekst = r"gruszkabananjabłkobananjabłkojabłkobananjabłkogruszka"

print(wzor)
print(tekst)

print(re.match(wzor, tekst))

if re.match(r".*" + wzor + r".*", tekst):
    print("Dopasowano!")
else:
    print("Nie dopasowano!")

if re.search(wzor, tekst):
    print("Search znalazl wzor!")
else:
    print("Search NIE znalazl wzor!")

print(re.findall(wzor, tekst))

dopasowanie = re.search(wzor, tekst)
if dopasowanie:
    print(dopasowanie.group())
    print(dopasowanie.start())
    print(dopasowanie.end())
    print(dopasowanie.span())


tekst2 = re.sub(wzor, r"jagoda", tekst)
print(tekst2)
'''

# wyrażenia regularne cz.2

import re
'''
if re.match("^[Kk]o.$", "Kot"):
    print("Dopasowano 1!")
else:
    print("Nie dopasowano 1!")


if re.match("^[A-Z]o.$", "kot"):
    print("Dopasowano 2!")
else:
    print("Nie dopasowano 2!")


if re.match("^[0-9]o.$", "5ot"):
    print("Dopasowano 3!")
else:
    print("Nie dopasowano 3!")


if re.match("^[^0-9]o.$", "5ot"):
    print("Dopasowano 4!")
else:
    print("Nie dopasowano 4!")


if re.match("^[rR]ok[-_=][0-9][0-9][0-9][0-9]$", "Rok-1984"):
    print("Dopasowano 5!")
else:
    print("Nie dopasowano 5!")
'''

# wyrażenia regularne cz.3

'''
print("\n")

if re.match("^[A-Z][a-z]$","Ala"):
    print("Dopasowano - cz.3.1")
else:    
    print("Niedopasowano - cz.3.1")


print("Walidacja imienia: ")
if re.match("^[A-Z][a-z]*$","Ala"):
    print("Dopasowano - cz.3.2")
else:    
    print("Niedopasowano - cz.3.2")


print("Walidacja imienia 2: ")
if re.match("^[A-Z][a-z]+$","A"):
    print("Dopasowano - cz.3.3")
else:    
    print("Niedopasowano - cz.3.3")


if re.match("^[A-Z][a-z]?[A-Z]$","Ala"):
    print("Dopasowano - cz.3.4")
else:    
    print("Niedopasowano - cz.3.4")

print("Pełna walidacja imienia: ")
if re.match("^[A-Z][a-z]{2,5}$","Sebastian"):
    print("Dopasowano - cz.3.5")
else:    
    print("Niedopasowano - cz.3.5")
'''

# wyrażenia regularne 4

'''
import re

wynik = re.match(r"^(?:He(?P<first>ll)o)( world)+(!|\.)$", "Hello world world world!")


if wynik:
    print("Dopasowano")
    print(wynik.group())
    print(wynik.group(0))
    print(wynik.group(1))
    print(wynik.group(2))
    print(wynik.group(3))
    print(wynik.groups())
    print(wynik.group("first"))
else:    
    print("NIE dopasowano")


print("Walidacja email: \n")
if re.match(r"^([A-Za-z0-9]+|[A-Za-z0-9][A-Za-z0-9\.-]+[A-Za-z0-9])@([A-Za-z0-9]+|[A-Za-z0-9][A-Za-z0-9-\.]+[A-Za-z0-9])\.[A-Za-z0-9]+$", "a@o.pl"):
    print("Dopasowano maila")

else:    
    print("NIE dopasowano maila")
'''

# argumenty funkcji (lista parametrów)
'''
def funkcja(arg1, arg2 = "World", *args, **kwargs):
    print(arg1, arg2, args, len(args), kwargs)
    for x in args:
        print(x)
    for x in kwargs.values():
        print(x)

    

funkcja("Hello")
funkcja("Hi", "youtube")
funkcja("Hi", "youtube", "!", ":-)", autor="Sebastian", rok="2021")
'''

# rozpakowanie krotki
'''
a, b = (2, 5)
print(a)
print(b)

c, d = [2, 5]
print(c)
print(d)

x = 10
y = 20
print("x = {}, y = {}".format(x, y))
x, y = y, x
print("x: ", x)
print("y: ", y)


start, *wszystko, koniec = (1,2,3,4,5,6,7)
print(start)
print(wszystko)
print(koniec)
'''

# skrócony operator IF


print("Prawda") if 5 > 2 else print("Nieprawda")

a = "Parzysta" if 11 % 2 == 0 else "Nieparzysta."
print(a)

for i in range(10):
    if i > 5:
        break
        #continue
else:
    print("Koniec 1")


try:
    a = 5/0

except ZeroDivisionError:
    print("błąd - dzielenie przez zero")
    
else:
    print("Koniec 2")




























