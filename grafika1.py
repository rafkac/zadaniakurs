# -*- coding: utf-8 -*-
# RafKac
# 2021_07_12
# 2021_07_15


# grafika w Pythonie - klasa Turtle
# podstawy
# rysowanie kwiatka
# trójkąty Sierpińskiego

import turtle
import random

#metoda rysująca zwykły kwadrat, zmienna typu turtle jest pierwszym parametrem,
# długośc boku oraz kąt jest drugim, zatem możemy rysować także inne czworoboki
def kwadrat(nazwa, dlugosc, kat):
    for i in range(4):
        nazwa.pensize(1)
        nazwa.forward(dlugosc)
        nazwa.left(kat)


# metoda dla nieco trudniejszych rysunków - rysuje specyficzną rozetę
def skomplikowany(nazwa):
    for i in range(300):
        nazwa.pensize(1)
        nazwa.forward(i)
        nazwa.right(88)


# pierwszy agrgument typu Turtle, drugi to int
def rysuj_luk(nazwa, dlugosc):
    n = int(dlugosc/2)
    for i in range(n):
        nazwa.forward(dlugosc/n)
        nazwa.left(60/n)

def rysuj_platek(turtel, dlugosc):
    rysuj_luk(turtel, dlugosc)
    turtel.left(120)
    rysuj_luk(turtel, dlugosc)

def rysuj_kwiat(turtel, dlugosc, ilosc):
    for i in range(ilosc):
        rysuj_platek(turtel, dlugosc)
        turtel.left(360/40)


# trójkąty Sierpińskiego
# słuzy do przesunięcia się w zadany punkt bez zostawienia śladu
def przesuniecie(turtel, x, y):
    turtel.up()
    turtel.goto(x,y)
    turtel.down()
    
# argumenty to Turtle oraz lista punktów
def rysuj_trojkat(turtel, punkty, kolor):
    przesuniecie(turtel, punkty[0][0], punkty[0][1])
    turtel.fillcolor(kolor)
    turtel.begin_fill()
    turtel.goto( punkty[1][0], punkty[1][1])
    turtel.goto( punkty[2][0], punkty[2][1])
    turtel.goto( punkty[0][0], punkty[0][1])
    turtel.end_fill()
    
def wyznacz_srodek(p1, p2):
    return ((p1[0] + p2[0])/2, (p1[1] + p2[1])/2)


def sierpinski(turtel, punkty, stopnie):
    lista_kolorow = ['red', 'violet', 'green', 'blue', 'grey', 'orange', 'yellow']
    rysuj_trojkat(turtel, punkty, lista_kolorow[random.randint(0, len(lista_kolorow)-1)])
    if(stopnie > 0):
        sierpinski(turtel, (punkty[0], wyznacz_srodek(punkty[0], punkty[1]), wyznacz_srodek(punkty[0], punkty[2])), stopnie - 1)
        sierpinski(turtel, (punkty[1], wyznacz_srodek(punkty[0], punkty[1]), wyznacz_srodek(punkty[1], punkty[2])), stopnie - 1)
        sierpinski(turtel, (punkty[2], wyznacz_srodek(punkty[2], punkty[1]), wyznacz_srodek(punkty[0], punkty[2])), stopnie - 1)
    print("Sierpiński.")




#zolwik = turtle.Turtle()
zolw = turtle.Turtle()

# zmniejszamy prędkość żółwika
#zolwik.speed(3)
#zmiana koloru rysunku
#zolwik.pencolor('blue')
#zmiana grubosci kreski
#zolwik.pensize(10)
#przesunięcie o zadeklarowaną liczbę kroków
#zolwik.forward(100)
#skręt w lewo o zadeklarowaną liczbę stopni
#zolwik.left(90)
#zolwik.forward(100)
#zolwik.left(45)
#zolwik.forward(100)
#zolwik.pencolor("black")
#kwadrat(zolwik, 100, 90)
#kwadrat(zolwik, 200, 90)

#skomplikowany(zolwik)



print("Rysowanie kwiatka: ")
zolw.speed(0)
#rysuj_luk(zolw, 100)
#rysuj_platek(zolw, 100)
'''rysuj_kwiat(zolw, 100, 13)
przesuniecie(zolw, -200,30)
rysuj_kwiat(zolw, 100, 13)
przesuniecie(zolw, -100, -200)
rysuj_kwiat(zolw, 100, 13)
przesuniecie(zolw, 100, -200)
rysuj_kwiat(zolw, 100, 13)
przesuniecie(zolw, 200, 30)
rysuj_kwiat(zolw, 100, 13)
przesuniecie(zolw, 0, 200)
rysuj_kwiat(zolw, 100, 13)
#przesuniecie(zolw, 150, 0)
#rysuj_kwiat(zolw, 100, 3)
'''

print("Trójkąt Sierpińskiego:")
lista_punktow = [[-100, -50], [0, 100], [100, -50]]
punkty2 = [[-200, -100], [0, 200], [200, -100]]
#rysuj_trojkat(zolw, lista_punktow)

sierpinski(zolw, punkty2, 8)



# żeby zamknął dopiero po kliknięciu, czyli żeby czekał na zamknięcie
turtle.exitonclick()
