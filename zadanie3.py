#stare formatownie stringow
szer = 42
print("-" * szer)
print("|  Czas  |     Zawodnik     |    Data    |")
print("*" * szer)
print("| %6.3f | %16s | %10s |" % (9.58, "Usain Bolt", "16.08.2009"))
print("| %6.3f | %16s | %10s |" % (9.69, "Tyson Gay", "20.09.2009"))
print("| %6.3f | %16s | %10s |" % (9.69, "Yohan Blake", "23.09.2012"))      
print("| %6.3f | %16s | %10s |" % (9.74, "Asafa Powell", "2.09.2008"))      
print("-" * szer)

#nowe formatowanie stringow

szer = 42
print("-" * szer)
print("|  Czas  |     Zawodnik     |    Data    |")
print("*" * szer)
print("| {:6.3f} | {:16s} | {:10s} |" .format(9.58, "Usain Bolt", "16.08.2009"))
print("| {:6.3f} | {:16s} | {:10s} |" .format(9.69, "Tyson Gay", "20.09.2009"))
print("| {:6.3f} | {:16s} | {:10s} |" .format(9.69, "Yohan Blake", "23.09.2012"))
print("| {:6.3f} | {:16s} | {:10s} |" .format(9.74, "Asafa Powell", "2.09.2008"))
print("-" * szer)
#zmieniamy wyjustowanie
print("| %6.3f | %-16s | %-10s |" % (9.58, "Usain Bolt", "16.08.2009"))
#zmieniamy ilosc miejsc po przecinku
print("| %06.1f | %16s | %10s |" % (9.58, "Usain Bolt", "16.09.2009"))

#wyswietlanie zmiennych
waluta = "dolar"
us = 1
pln = 4.08234915
print("Aktualnie %d %s kosztuje %.2f zl" % (us, waluta, pln))
#nowe formatowanie
print("Aktualnie {} {} kosztuje {:3.2f} zl".format(us, waluta, pln))


#zadanie 1 z tej lekcji - konwerter jednostek
print("Podaj dlugosc (w cm) oraz wage (w kg):")
dlugosc = int(input())
metry = float(dlugosc/100.0)
cale = float(dlugosc/2.54)
waga = int(input())
funty = float(waga/0.45)
print("{} cm to {} cali oraz {} metrów, {} kg to {} funtów".format(dlugosc, cale, metry, waga, funty))

#zadanie 2 z tej lekcji - obliczanie stanu konta za parę lat
print("*"*10, "lokata ", "*" * 10)
print("Podaj stan poczatkowy konta:")
poczatek = int(input())
print("podaj stope oprocentowania rocznego")
stopa = float(input())
print("Podaj liczbe lat na lokacie")
lata = int(input())

print("Twoje {} zl przez {} lata na lokacie {}% urosnie do {}".format(poczatek, lata, stopa, poczatek*(1 + lata * stopa/100.0)))
