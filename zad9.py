# -*- coding: utf-8 -*-
# by RafKac
# 2021_06_01
# try-catch, raise, assert, operacje na plikach tekstowych


import random
from math import pi
from math import sqrt as pierwiastek

def funkcja():
    print("Podaj liczbe")
    i = int(input())

    return i


def dodaj(x, y = 20, z = 0):
    return x + y + z


def potega(x, y = 2):
    return x**y


def silnia(x):
    if(x <= 1):
        return 1
    else:
        return x * silnia(x-1)


def silniaIteracyjna(x):
    if (x == 0):
        return 1
    wynik = 1
    for i in range(1, x+1):
        wynik = wynik * i
    return wynik    

def dzielenie(x, y):
    assert not y == 0, "Y == 0"
    if y == 0:
        raise ZeroDivisionError("Dzielenie przez 0")
    print(x/y)
    

print("\nPoczątek")
#print(dodaj(funkcja(),funkcja()))
#q = potega
#print("potega: {}".format(q(4,2)))

#p = int(input("Z jakiej liczby policzyć silnię?  "))
#print("Silnia {}! iteracyjnie: {}, rekurencyjnie: {}".format(p, silniaIteracyjna(p), silnia(p)))

#print(random.randint(1,10))
#print(pi)
#print(pierwiastek(16))

#obsługa wyjątków
#x = 12
#y = 0
#try:
#    lista = []
   # print(lista[0])
   # print(x + "!")
   # print(x/y)
#    print("Linia po operacjach (koniec bloku <<try>>)")
#    dzielenie(x,y)
#except (ZeroDivisionError, TypeError):
#    print("1 z dwóch wyjątków! ")
#    print("\nwyjątek - dzielenie przez zero!")
#except TypeError:
#    print("\nwyjątek - TypeError")
#except:
#    print("\nInny błąd \n")
#finally:
#    print("Finally")

# obsługa plików
plik = open("test.txt", "a")
if(plik.writable()):
    plik.write(input("Podaj tekst: ") + "\n")
plik.close()

plik = open("test.txt","r")
if(plik.readable()):
#    print("\nzawartość pliku: ")
#    tekst = plik.read()
#    print(tekst)
#
#if(plik.readable()):
    print("\nZawartość pliku: ")
    #tekst = plik.readlines()
    #print(tekst)
    #for l in tekst:
    #    print(l)
    #    print()

    #możemy jeszcze innaczej:
    for l in plik:
        print(l)

plik.close()
print("Dalsze instrukcje\n ")
    


