""" Zadanie 4- to zadania 1 i 2 z lekcji 4 z kursu flunerd.pl
    13 maja 2021
    1) tworzenie i wyswietlanie dowolnych zmiennych przechowujacych wartosci ponizszych typow
    2) "spis ogladanych seriali z ocena 1-10, z pytaniem do usera, co chce ogladac, w odpowiedzi podajemy ocene"
        user moze dodac kolejny serial i ocene, serial dodajemy do spisu
"""
isGood = False
int01 = 1234
number01 = 0.943
text = "Text dla testu"
list01 = ["babcia", "mama", "tata", "siostra", "brat", "dziadek", "stryjek", "wojek", "ciocia"]
tuple01 = ("chlopak", "dziewczyna")
dict01 = {0:"mama", 1:"tata", 2:"babcia", 3:"dziadek", 4:"siostra", 5:"brat"}

print(isGood)
print(int01)
print(number01)
print(text)
print(list01)
print(tuple01)
print(dict01)

#druga czesc zadania

print("****************************************")
print("Seriale (wybierz, który chcesz oglądać):")
slownik1 = {"Flash":4, "Lost":5, "Big Bang Theory":6, "Black mirror":7, "The crown":5, "Star wars:Rebels":6}
print(list(slownik1.keys()))
print("****************************************")
iWant = input("Jaki chcesz obejrzec? ")
print(slownik1[iWant])
print("Serial {} otrzymał ocenę: {}".format(iWant, slownik1[iWant]))
print("****************************************")
print("Podaj nowy serial i jego ocenę, aby dodać do bazy ")
serial = input("Tytuł serialu? ")
ocena = input("Twoja ocena? ")
slownik1[serial]=float(ocena)
print(slownik1)
print("****************************************")
print(slownik1.keys())

