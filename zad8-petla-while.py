# -*- coding: utf8 -*-
# lekcja ósma

# Zad 1 - suma poprzedników z pętlą while
'''
max = 10
i = 0
while(i <= 10):
    suma = 0
    j = 0
    while(j <= i):
        suma +=  j
        j += 1
    i += 1
    print(suma, end = " ")
    
'''

# Zad 2 - gra zgadywanka (i zadanie 3 - podpowiedź o zakresie liczby)
'''
import random

prob = random.randrange(1,31)
jeszcze = True
while(jeszcze):
    odp = int(input("Podaj Twoje trafienie (1, 30): "))
    if(odp == prob):
        jeszcze = False
        print("Trafiłeś - szukana liczba to {}.".format(prob))
    else:
        if(odp > prob):
            print("Trafienie jest za duże.")
        else:
            print("Trafienie jest za małe.")        
'''

# zad 4 - silnia liczona iteracyjnie
'''
n = int(input("Podaj dowolną liczbę całkowitą do 15: "))
wynik = 1
for i in range(1,n):
    wynik = wynik * i

print("{}! = {}".format(n,wynik))
'''

# Zad 5 - prosta gra anagramowa

import random

slowa = ["ameba", "pociąg", "poczta", "manekin", "papaja", "Kingulińska", "Konstantynopol", "Zapolice", "Kaszubskie Golemy", "Starorypińskie", "hipopotamy" ]

los = random.randrange(0,len(slowa))
slowo = slowa[los]


#mieszanie liter
wynik = slowo
for i in range(1,len(slowo)):
    el = random.randrange(1,len(slowo)-1)
    wynik = wynik[:el] + wynik[(el+1):] + wynik[el]
#    print(wynik)

print("Wymieszane litery: {}".format(wynik))

while(True):
    odp = input("Podaj odpowiedź, wciśnij q (lub Q), aby zakończyć: ")
    if(odp == "q" or odp == "Q"):
        print(" ****** KONIEC ****** ")
        break
    else:
        if(odp == slowo):
            print("Dobrze!")
            break
        







